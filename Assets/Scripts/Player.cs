﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	private bool isDead = false;
	private Rigidbody2D rigid2d;

	// Use this for initialization
	void Start () {
		rigid2d = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!isDead && Input.GetMouseButtonUp (0)) {
			rigid2d.velocity = Vector2.zero;
			rigid2d.AddForce (new Vector2 (0, GameControl.instance.jumpSpeed));
		}
	}

	void OnCollisionEnter2D(Collision2D other) {
		isDead = true;
		rigid2d.velocity = Vector2.zero;
		GameControl.instance.PlayerDied ();
	}
}
