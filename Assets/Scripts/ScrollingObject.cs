﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingObject : MonoBehaviour {

	private Rigidbody2D rigid2d;
	private float lastScrolingSpeed;

	// Use this for initialization
	void Start () {
		rigid2d = GetComponent<Rigidbody2D> ();
		lastScrolingSpeed = GameControl.instance.scrollingSpeed;
		rigid2d.velocity = new Vector2 (lastScrolingSpeed, 0);
	}
	
	// Update is called once per frame
	void Update () {
		if (GameControl.instance.gameOver)
			rigid2d.velocity = Vector2.zero;
		else
			if (lastScrolingSpeed != GameControl.instance.scrollingSpeed) {
				lastScrolingSpeed = GameControl.instance.scrollingSpeed;
				rigid2d.velocity = new Vector2 (lastScrolingSpeed, 0);
			}
			
	}
}
