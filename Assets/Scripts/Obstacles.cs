﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour {

	public int obstaclesPoolSize = 5;
	public GameObject[] obstaclesPrefabs;
	public float spawnRate = 4f;
	public float spawnMin = -1f;
	public float spawnMax = 3.5f;

	private GameObject[,] obstacles;
	private Vector2 poolPosition = new Vector2 (-25f, -25f);
	private float timeSinceLastSpawned;
	private int currentIndex = 0;
	private float localSpawnRate = 0;

	// Use this for initialization
	void Start () {
		obstacles = new GameObject[obstaclesPrefabs.Length, obstaclesPoolSize];
		for (int i = 0; i < obstaclesPrefabs.Length; i++) {
			for (int j = 0; j < obstaclesPoolSize; j++) {
				obstacles[i, j] = Instantiate(obstaclesPrefabs[i], poolPosition, Quaternion.identity);
			}
		}
		localSpawnRate = Random.Range (0.5f, spawnRate);

	}
	
	// Update is called once per frame
	void Update () {
		timeSinceLastSpawned += Time.deltaTime;

		if (!GameControl.instance.gameOver && timeSinceLastSpawned >= localSpawnRate) {
			timeSinceLastSpawned = 0;
			localSpawnRate = Random.Range (2, spawnRate);

			float spawnYPosition = Random.Range (spawnMin, spawnMax);
			obstacles [0, currentIndex++].transform.position = new Vector2 (10f, spawnYPosition);
			if (currentIndex >= obstaclesPoolSize)
				currentIndex = 0;

		}
		
	}
}
