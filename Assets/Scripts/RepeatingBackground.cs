﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackground : MonoBehaviour {

	private float backgroundHsize;

	// Use this for initialization
	void Start () {
		backgroundHsize = GetComponent<BoxCollider2D> ().size.x;
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < -backgroundHsize) {
			transform.position = new Vector2(transform.position.x + backgroundHsize * 2f, transform.position.y);
		}
	}
}
