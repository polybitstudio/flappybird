﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

	public static GameControl instance;
	public GameObject gameOverText;
	public Text scoreText;
	public bool gameOver = false;
	public float scrollingSpeed = -1.5f;
	public float jumpSpeed = 10f;

	private int _score = 0;

	// Use this for initialization
	void Awake () {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		if (gameOver && Input.GetMouseButtonUp (0))
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}


	public void PlayerScored(int score) {
		if (!gameOver) {
			_score += score;
			scoreText.text = "SCORE : " + _score.ToString ();
			if (score == 1) {
				scrollingSpeed += -0.05f;
			}
		}
	}


	public void PlayerDied() {
		gameOverText.SetActive (true);
		gameOver = true;
	}
}
